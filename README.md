Fungsi utama git adalah untuk mengatur versi dari source code pengguna, menambahkan checkpoint ketika terjadi perubahan pada kode pengguna dan tentunya akan mempermudah pengguna untuk tetap mengetahui apa saja yang berubah dari source code pengguna , selain itu jika pengguna ingin mengembalikan Project anda dari awal pengguna bisa mengambil source code tersebut di Git.

Yang dilakukan git itu sebenarnya akan memantau semua perubahan yang terjadi pada file proyek,lalu menyimpannya ke dalam database.

Manfaat Git yaitu sebagai berikut ;

·        Bisa menyimpan seluruh versi source code

·        Bisa paham cara berkolaborasi dalam proyek

·        Bisa ikut berkonstribusi ke proyek open-source

·        Bisa memahami cara deploy aplikasi modern

·        Bisa membuat blod dengan SSG
